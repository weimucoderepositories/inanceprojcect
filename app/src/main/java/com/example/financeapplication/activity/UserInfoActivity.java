package com.example.financeapplication.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.financeapplication.R;
import com.example.financeapplication.common.BitmapUtils;
import com.example.financeapplication.common.ImageFileUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class UserInfoActivity extends AppCompatActivity implements View.OnClickListener{

    private final int PICTUER = 100;
    private final int CAMERA = 101;
    private ImageView navLeftbackImg;
    private TextView navTitleTv;
    private ImageView navSettingImg;
    private ImageView ivUserIcon;
    private TextView tvUserChange;
    private Button btnUserLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        initView();
    }

    private void initView() {
        navLeftbackImg = (ImageView) findViewById(R.id.nav_leftback_img);
        navTitleTv = (TextView) findViewById(R.id.nav_title_tv);
        navSettingImg = (ImageView) findViewById(R.id.nav_setting_img);

        navSettingImg.setVisibility(View.INVISIBLE);
        navLeftbackImg.setVisibility(View.VISIBLE);
        navTitleTv.setText("个人信息");

        ivUserIcon = (ImageView) findViewById(R.id.iv_user_icon);
        tvUserChange = (TextView) findViewById(R.id.tv_user_change);
        btnUserLogout = (Button) findViewById(R.id.btn_user_logout);

        navLeftbackImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserInfoActivity.this.finish();
            }
        });

        tvUserChange.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
         String[] items = new String[]{"图库","相机"};
         new AlertDialog.Builder(this)
                 .setTitle("选择图片来源")
                 .setItems(items, new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                          if (which == 0){//图库

                              if (ContextCompat.checkSelfPermission(UserInfoActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                                  ActivityCompat.requestPermissions(UserInfoActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},PICTUER);
                              }else {
                                  takePhoto();
                              }

                          }else {//相机
                              if (ContextCompat.checkSelfPermission(UserInfoActivity.this,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                              ContextCompat.checkSelfPermission(UserInfoActivity.this,Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                                  ActivityCompat.requestPermissions(UserInfoActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA},CAMERA);
                              }else {
                                  takeCamera();
                              }


                          }
                     }
                 }).setCancelable(true)
                 .create()
                 .show();

    }



    private void takePhoto(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent,PICTUER);
    }
    private void takeCamera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
         if (requestCode == PICTUER){
             if (grantResults.length >= 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                 takePhoto();
             }
         }else if (requestCode == CAMERA){
             if (grantResults.length >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                 takeCamera();
             }

         }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICTUER && resultCode == RESULT_OK && data != null){ //图片

            Uri imgUri = data.getData();
            String filePath = ImageFileUtil.getFilePathByUri(this,imgUri);
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            Bitmap zoomBitmap = BitmapUtils.zoom(bitmap,ivUserIcon.getWidth(),ivUserIcon.getHeight());
            zoomBitmap = BitmapUtils.circleBitmap(zoomBitmap);
            ivUserIcon.setImageBitmap(zoomBitmap);
            saveImage(zoomBitmap);
        }else if (requestCode == CAMERA && resultCode == RESULT_OK && data != null){ //相机
            Bundle extras = data.getExtras();
            Bitmap bitmap = (Bitmap) extras.get("data");

            Bitmap zoomBitmap = BitmapUtils.zoom(bitmap,ivUserIcon.getWidth(),ivUserIcon.getHeight());
            zoomBitmap = BitmapUtils.circleBitmap(zoomBitmap);
            bitmap.recycle();
            ivUserIcon.setImageBitmap(zoomBitmap);
            saveImage(zoomBitmap);
        }
    }

    private void saveImage(Bitmap bitmap){

        String filepath = null;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
             filepath = getExternalFilesDir("").getAbsolutePath() + "/" + "test.png";
        }else {
            //获取绝对路径
            filepath = getFilesDir().getAbsolutePath() + "/" + "test.png";
        }

        FileOutputStream outputStream = null;
        try {
            File file = new File(filepath);
            outputStream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.PNG,100,outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}