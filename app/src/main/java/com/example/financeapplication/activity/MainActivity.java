package com.example.financeapplication.activity;

import android.app.ActivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.financeapplication.R;
import com.example.financeapplication.fragment.HomeFragment;
import com.example.financeapplication.fragment.InvestFragment;
import com.example.financeapplication.fragment.MoreFragment;
import com.example.financeapplication.fragment.MyMoneyFragment;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private HomeFragment homeFragment;
    private InvestFragment investFragment;
    private MoreFragment moreFragment;
    private MyMoneyFragment myMoneyFragment;
    private FrameLayout mainContentView;
    private LinearLayout llMainHome;
    private ImageView imgMainHome;
    private TextView tvMainHome;
    private LinearLayout llMainInvest;
    private ImageView imgMainInvest;
    private TextView tvMainInvest;
    private LinearLayout llMainMymoney;
    private ImageView imgMainMymoney;
    private TextView tvMainMymoney;
    private LinearLayout llMainMore;
    private ImageView imgMainMore;
    private TextView tvMainMore;

    private FragmentTransaction transaction;
    private boolean flag = true;
    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message message) {
            if (message.what == 10){
                flag = true;
            }
            return false;
        }
    });


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        setSelectTab(0);


    }

    private void initView() {
        mainContentView = (FrameLayout) findViewById(R.id.main_content_view);
        llMainHome = (LinearLayout) findViewById(R.id.ll_main_home);
        imgMainHome = (ImageView) findViewById(R.id.img_main_home);
        tvMainHome = (TextView) findViewById(R.id.tv_main_home);
        llMainInvest = (LinearLayout) findViewById(R.id.ll_main_invest);
        imgMainInvest = (ImageView) findViewById(R.id.img_main_invest);
        tvMainInvest = (TextView) findViewById(R.id.tv_main_invest);
        llMainMymoney = (LinearLayout) findViewById(R.id.ll_main_mymoney);
        imgMainMymoney = (ImageView) findViewById(R.id.img_main_mymoney);
        tvMainMymoney = (TextView) findViewById(R.id.tv_main_mymoney);
        llMainMore = (LinearLayout) findViewById(R.id.ll_main_more);
        imgMainMore = (ImageView) findViewById(R.id.img_main_more);
        tvMainMore = (TextView) findViewById(R.id.tv_main_more);

        llMainHome.setOnClickListener(this);
        llMainInvest.setOnClickListener(this);
        llMainMymoney.setOnClickListener(this);
        llMainMore.setOnClickListener(this);

    }

    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.ll_main_home:
                setSelectTab(0);
                break;
            case R.id.ll_main_invest:
                setSelectTab(1);
                break;
            case R.id.ll_main_mymoney:
                setSelectTab(2);
                break;
            case R.id.ll_main_more:
                setSelectTab(3);
                break;
        }
    }
   private void setSelectTab(int i){
       FragmentManager manager = getSupportFragmentManager();
       transaction = manager.beginTransaction();
       //隐藏其他frament
       hiddenFragment();
       //设置tab选中的按钮
       setTabImageAndTV();
       switch (i) {
           case 0:
               if (homeFragment == null) {
                   homeFragment = new HomeFragment();
                   transaction.add(R.id.main_content_view, homeFragment);
               }
               transaction.show(homeFragment);
               imgMainHome.setImageResource(R.drawable.bottom02);
               tvMainHome.setTextColor(ContextCompat.getColor(this,R.color.home_back_selected));

               break;
           case 1:
               if (investFragment == null) {
                   investFragment = new InvestFragment();
                   transaction.add(R.id.main_content_view, investFragment);
               }
               transaction.show(investFragment);
               imgMainInvest.setImageResource(R.drawable.bottom04);
               tvMainInvest.setTextColor(ContextCompat.getColor(this,R.color.home_back_selected));

               break;
           case 2:
               if (myMoneyFragment == null) {
                   myMoneyFragment = new MyMoneyFragment();
                   transaction.add(R.id.main_content_view, myMoneyFragment);
               }
               transaction.show(myMoneyFragment);
               imgMainMymoney.setImageResource(R.drawable.bottom06);
               tvMainMymoney.setTextColor(ContextCompat.getColor(this,R.color.home_back_selected01));

               break;
           case 3:
               if (moreFragment == null) {
                   moreFragment = new MoreFragment();
                   transaction.add(R.id.main_content_view, moreFragment);
               }
               transaction.show(moreFragment);
               imgMainMore.setImageResource(R.drawable.bottom08);
               tvMainMore.setTextColor(ContextCompat.getColor(this,R.color.home_back_selected));

               break;
       }
       transaction.commit();
   }

   public void hiddenFragment(){
       if (homeFragment != null ) {
           transaction.hide(homeFragment);
       }
       if (investFragment != null) {
           transaction.hide(investFragment);
       }
       if (myMoneyFragment != null ) {
           transaction.hide(myMoneyFragment);
       }
       if (moreFragment != null ) {
           transaction.hide(moreFragment);
       }
   }

    public void setTabImageAndTV(){
        imgMainHome.setImageResource(R.drawable.bottom01);
        tvMainHome.setTextColor(ContextCompat.getColor(this,R.color.home_back_unselected));
        imgMainInvest.setImageResource(R.drawable.bottom03);
        tvMainInvest.setTextColor(ContextCompat.getColor(this,R.color.home_back_unselected));
        imgMainMymoney.setImageResource(R.drawable.bottom05);
        tvMainMymoney.setTextColor(ContextCompat.getColor(this,R.color.home_back_unselected));
        imgMainMore.setImageResource(R.drawable.bottom07);
        tvMainMore.setTextColor(ContextCompat.getColor(this,R.color.home_back_unselected));

    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && flag){
            flag = false;
            Toast.makeText(this,"再点击将退出应用",Toast.LENGTH_SHORT).show();
            Message message = new Message();
            message.what = 10;
            handler.sendMessageDelayed(message,1000 * 2);
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //防止内存泄露 移出所有消息
        handler.removeCallbacksAndMessages(null);
    }
}