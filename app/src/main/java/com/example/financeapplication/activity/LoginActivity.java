package com.example.financeapplication.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.financeapplication.R;
import com.example.financeapplication.beans.User;
import com.example.financeapplication.common.ActivityManager;
import com.example.financeapplication.common.AppNetConfig;
import com.example.financeapplication.common.MD5Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.BufferedSink;

public class LoginActivity extends AppCompatActivity implements  View.OnClickListener {

    private ImageView navLeftbackImg;
    private TextView navTitleTv;
    private ImageView navSettingImg;
    private RelativeLayout rlLogin;
    private TextView tvLoginNumber;
    private EditText etLoginNumber;
    private TextView tvLoginPwd;
    private EditText etLoginPwd;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        navLeftbackImg = (ImageView) findViewById(R.id.nav_leftback_img);
        navTitleTv = (TextView) findViewById(R.id.nav_title_tv);
        navSettingImg = (ImageView) findViewById(R.id.nav_setting_img);

        navSettingImg.setVisibility(View.INVISIBLE);
        navLeftbackImg.setVisibility(View.VISIBLE);
        navTitleTv.setText("登录");

        rlLogin = (RelativeLayout) findViewById(R.id.rl_login);

        tvLoginNumber = (TextView) findViewById(R.id.tv_login_number);
        etLoginNumber = (EditText) findViewById(R.id.et_login_number);
        tvLoginPwd = (TextView) findViewById(R.id.tv_login_pwd);
        etLoginPwd = (EditText) findViewById(R.id.et_login_pwd);
        btnLogin = (Button) findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(this);
        navLeftbackImg.setOnClickListener(this);
        SharedPreferences sharedPreferences = getSharedPreferences("user_info", Context.MODE_PRIVATE);
        String phone = sharedPreferences.getString("phone","");
        etLoginNumber.setText(phone);

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.nav_leftback_img){
            this.finish();
            return;
        }

        String phone = etLoginNumber.getText().toString();
        String password = etLoginPwd.getText().toString();
        if (TextUtils.isEmpty(phone) || TextUtils.isEmpty(password)){
            Toast.makeText(this,"用户名或者密码不能为空",Toast.LENGTH_SHORT).show();
            return;
        }
        password = MD5Utils.MD5(password);
        OkHttpClient client = new OkHttpClient();
        String reqeustURl = AppNetConfig.login_url;

        FormBody body = new FormBody.Builder()
                .add("phone",phone)
                .add("password",password)
                .build();

        final Request request = new Request.Builder()
                .post(body)
                .url(reqeustURl)
                .build();
        Call call = client.newCall(request);
        String finalPassword = password;
        call.enqueue(new Callback() {


            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                   String responseStr = response.body().string();
                try {
                    JSONObject jsonObject = new JSONObject(responseStr);
                    boolean success = jsonObject.getBoolean("success");

                    if (success) {
                        String dataStr = jsonObject.getString("data");
                        Gson gson = new Gson();
                        User user = gson.fromJson(dataStr, new TypeToken<User>() {
                        }.getType());

                        SharedPreferences sharedPreferences = getSharedPreferences("user_info", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("name", user.getName());
                        editor.putString("password", finalPassword);
                        editor.putString("phone", user.getPhone());
                        editor.putString("imageurl", user.getImageurl());
                        editor.commit();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ActivityManager.getInstance().removeAll();
                                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                               startActivity(intent);

                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("TAG", "onResponse: " + responseStr);



            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.d("TAG", "onFailure: " + e.toString());
            }

        });
    }
}