package com.example.financeapplication.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;

import com.example.financeapplication.R;

public class WelcomeActivity extends AppCompatActivity {

    private FrameLayout frameLayout;
    private  Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            if (msg.what == 1000){
                Intent intent = new Intent(WelcomeActivity.this,MainActivity.class);
                startActivity(intent);
            }
            return false;
        }
    });
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        frameLayout = findViewById(R.id.welcome_backgroud);
        //去掉状态栏
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

//        Message msg = new Message();
//        msg.what = 1000;
//        handler.sendMessageDelayed(msg,2 * 1000);



        AlphaAnimation alphaAnimation = new AlphaAnimation(0,1);
        alphaAnimation.setDuration(2 * 1000);
        alphaAnimation.setRepeatMode(0);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                handler.postDelayed(new Runnable() {  //并没有开启新线程
                    @Override
                    public void run() {
                        startMainActivity();
                    }
                },2 * 1000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        frameLayout.startAnimation(alphaAnimation);
    }

    private void startMainActivity(){
        Intent intent = new Intent(WelcomeActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void updateApkFile(){
        boolean isNetwork = isConnect();
        if (isNetwork){

        }else {
            ProgressDialog dialog = new ProgressDialog(this);
        }
    }
    public boolean isConnect(){
        boolean connected = false;
        ConnectivityManager manager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if (networkInfo != null){
            connected = networkInfo.isConnected();
        }
        return connected;

    }
}