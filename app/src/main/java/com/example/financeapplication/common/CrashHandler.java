package com.example.financeapplication.common;

import android.os.Build;
import android.os.Looper;
import android.os.Process;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

public class CrashHandler implements Thread.UncaughtExceptionHandler {

    //初始化单利
    private CrashHandler(){

    }
    private static CrashHandler crashHandler = new CrashHandler();
    public static CrashHandler getInstance(){
        return crashHandler;
    }

    private Thread.UncaughtExceptionHandler defaultExceptionHandler;
    public void init(){
        defaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    //这是方法是异步线程
    @Override
    public void uncaughtException(@NonNull Thread t, @NonNull Throwable e) {
        Log.d("TAG", "uncaughtException: " + "出现异常了" + t.getName() + e.getLocalizedMessage());


        String deviceInfo = "设备主板：" + Build.BOARD +
                "设备名：" + Build.DEVICE +
                "手机厂商：" + Build.BRAND +
                "系统版本：" + Build.VERSION.RELEASE +
                "手机型号：" + Build.MODEL;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare(); //这是调用到主线程
                Toast.makeText(UIUtils.getContext(), "出现异常了", Toast.LENGTH_SHORT).show();
                Looper.loop();
            }
        }).start();

        try{
            //异常当前activity
            ActivityManager.getInstance().removeCurrent();
            //杀死当前进程
            android.os.Process.killProcess(Process.myTid());
            //退出Android虚拟机
            System.exit(0);
        }catch (Exception ep){
            ep.printStackTrace();
        }
    }
}
