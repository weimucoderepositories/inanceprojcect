package com.example.financeapplication.common;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.Window;

import androidx.core.app.ServiceCompat;
import androidx.core.content.ContextCompat;

public class UIUtils {

    public static Context getContext(){
        return MyApplication.context;
    }

    public static Handler getHandler(){
        return MyApplication.handler;
    }
    //获取颜色
    public static int getColor(int colorId){
        return ContextCompat.getColor(getContext(),colorId);
    }

    public static View getView(int viewId){
        return  View.inflate(getContext(),viewId,null);
    }

    public static String[] getStringArr(int stringid){
        return getContext().getResources().getStringArray(stringid);
    }

    public static int db2px(int dp){
       float density =  getContext().getResources().getDisplayMetrics().density;
        return (int) (dp * density + 0.5); //四舍五入
    }

    public static int px2dp(int px){
        float density =  getContext().getResources().getDisplayMetrics().density;
        return (int) (px / density + 0.5);
    }


}
