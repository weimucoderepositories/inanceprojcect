package com.example.financeapplication.common;

import android.app.Activity;

import java.util.Stack;

public class ActivityManager {

    public Stack<Activity> activityStack = new Stack<>();
    private ActivityManager(){
    }

    private static ActivityManager activityManager = new ActivityManager();

    public static ActivityManager getInstance(){
        return activityManager;
    }

    //添加
    public void add(Activity activity){
        if (activity != null){
            activityStack.add(activity);
        }
    }
    //从栈空间 同一类的 activity 移出
    public void remove(Activity activity){
        if (activity != null){
            //这种写法有问题 stack的size 一直是变化的
//            for (int i = 0; i < activityStack.size(); i++) {
//                Activity tempActivity = activityStack.get(i);
//                if (tempActivity.getClass().equals(activity.getClass())){
//                    tempActivity.finish();
//                    activityStack.remove(i);
//
//                }
//            }
            //这样就能解决 size变化的问题
            for (int i = activityStack.size() - 1; i >= 0; i--) {
                Activity tempActivity = activityStack.get(i);
                if (tempActivity.getClass().equals(activity.getClass())) {
                    tempActivity.finish();
                    activityStack.remove(i);
                }
            }
        }
    }
    //移出当前activity
    public void removeCurrent(){
        Activity tempActivity = activityStack.lastElement();
        tempActivity.finish();
        activityStack.remove(tempActivity);
    }

    //移出所有activity
    public void removeAll(){
        //这样就能解决 size变化的问题
        for (int i = activityStack.size() - 1; i >= 0; i--) {
            Activity tempActivity = activityStack.get(i);
            tempActivity.finish();
            activityStack.remove(i);
        }
    }

    //移出所有activity
    public int getSize(){
        //这样就能解决 size变化的问题
         return activityStack.size();
    }
}
