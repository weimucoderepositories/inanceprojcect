package com.example.financeapplication.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ScrollView;

//一般来说 自定义viewgroup 是 onMeasure 和  onLayout  自定义view是写onDraw
public class MyScrollview extends ScrollView {
    protected  View childrenView;

    public MyScrollview(Context context) {
        super(context);
    }

    public MyScrollview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyScrollview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyScrollview(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
    }



    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        //因为scrollview 只有一个子视图 所以获取零 就可以
        if (getChildCount() > 0){
             childrenView = getChildAt(0);
        }

    }



    private int downX,downY;

    //事件拦截
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        boolean isIntercept = false;
        int eventX = (int) ev.getX();
        int eventY =  (int) ev.getY();
        switch (ev.getAction()){
            case MotionEvent.ACTION_DOWN:
                 downX=  eventX;
                 downY = eventY;
                break;
            case MotionEvent.ACTION_MOVE:
                int absX = Math.abs(eventX - downX);
                int absY = Math.abs(eventY - downY);
                if (absY > absX && absY > UIUtils.db2px(10)){
                    isIntercept = true;
                }
                break;
        }
         return isIntercept;
    }

    private int lastY;
    private Rect normalRect = new Rect();
    private boolean isAnimationFinish = true;
    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        if (childrenView == null || !isAnimationFinish){
            return super.onTouchEvent(ev);
        }
        //获取点击事件的y值  ev.getY()
        int eventY = (int)ev.getY();
        switch (ev.getAction()){
            case MotionEvent.ACTION_DOWN:
                  lastY = eventY;
                break;
            case MotionEvent.ACTION_MOVE:
                int dy = eventY - lastY;
                if (isNeedMove()){
                    if (normalRect.isEmpty()){
                        normalRect.set(childrenView.getLeft(),getTop() ,childrenView.getRight(),getBottom());
                    }
                    childrenView.layout(childrenView.getLeft(),getTop() + dy/2,childrenView.getRight(),getBottom() + dy/2);
                }
                lastY = eventY;
                break;
            case MotionEvent.ACTION_UP:
                if (isNeedAnimation()) {
                    int translateY = childrenView.getBottom() - normalRect.bottom;
                    TranslateAnimation translateAnimation = new TranslateAnimation(0,0,0,-translateY);
                    translateAnimation.setDuration(200);
                    translateAnimation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            isAnimationFinish = false;
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            isAnimationFinish = true;
                            childrenView.clearAnimation();
                            childrenView.layout(normalRect.left,normalRect.top ,normalRect.right,normalRect.bottom);
                            normalRect.setEmpty();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    childrenView.startAnimation(translateAnimation);

                }
                break;
        }

        return super.onTouchEvent(ev);
    }

    private boolean isNeedMove(){
        //children的高 是大于 scrollview的高的
        int childrenMeasureH = childrenView.getMeasuredHeight();
        int scrollViewMeasureH = this.getMeasuredHeight();
        int dy = childrenMeasureH - scrollViewMeasureH;
        int scrollY = this.getScrollY();
        if (scrollY < 0 || scrollY > dy){
            return true;
        }
      return false;
    }

    private boolean isNeedAnimation(){
         return !normalRect.isEmpty();
    }
}
