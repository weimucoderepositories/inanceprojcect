package com.example.financeapplication.common;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;

public class DrawUtils {


    @SuppressLint("WrongConstant")
    public static Drawable getDrawable(int rgb, float radius){
        //代替shape 的方法  是Java代码写的
        GradientDrawable gradientDrawable = new GradientDrawable();
//        gradientDrawable.setPadding();
        gradientDrawable.setColor(rgb);
        gradientDrawable.setGradientType(GradientDrawable.RECTANGLE);
        gradientDrawable.setCornerRadius(radius);
        //边框宽度和颜色
        gradientDrawable.setStroke(UIUtils.db2px(1),rgb);
        return gradientDrawable;
    }

    public static StateListDrawable getSelector(Drawable normalDrawable,Drawable pressDrawable){
        StateListDrawable stateListDrawable = new StateListDrawable();
        //给当前的颜色选择器添加选择图片的状态，未选中图片只向状态
        stateListDrawable.addState(new int[]{android.R.attr.state_enabled, android.R.attr.state_pressed},pressDrawable);
        stateListDrawable.addState(new int[]{android.R.attr.state_enabled},normalDrawable);
        //设置默认状态
        stateListDrawable.addState(new int[]{},normalDrawable);
        return stateListDrawable;
    }
}
