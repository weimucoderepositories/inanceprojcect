package com.example.financeapplication.common;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;

import javax.crypto.spec.PSource;

public class BitmapUtils {
    /**
     * 代码：功能性代码；非功能性代码。
     *
     * @param source
     * @return
     */
    public static Bitmap circleBitmap(Bitmap source){
        int width = source.getWidth();
        Bitmap bitmap = Bitmap.createBitmap(width,width,Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        Paint paint = new Paint();
        paint.setAntiAlias(true);
        canvas.drawCircle(width/2,width/2,width/2,paint);
        //setXfermode 设置绘制的图像出现相交情况时候处理方式 它包含的常用模式：
        // ProteDuff.Mode.SRC_IN 去两层图像交集部分，只显示上层图像
        //ProteDuff.Mode.DST_IN 去两层图像交集部分，只显示下层图像
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(source,0,0,paint);

        return bitmap;
    }
    //实现图片的压缩处理
    public static Bitmap zoom(Bitmap source,float width,float height){
        Matrix matrix = new Matrix();
        matrix.postScale(width/source.getWidth(),height/source.getHeight());
        Bitmap bitmap = Bitmap.createBitmap(source,0,0,source.getWidth(), source.getHeight(),matrix,false);
        return bitmap;
    }
}
