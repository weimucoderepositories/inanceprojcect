package com.example.financeapplication.common;

import android.content.Context;
 import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.List;

public class FlowLayout extends ViewGroup {

    public FlowLayout(Context context) {
        this(context,null);
    }

    public FlowLayout(Context context, AttributeSet attrs) {
        super(context, attrs,0);
    }

    public FlowLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //获取设置宽高的模式和具体的值
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        //如果用户使用的至多模式没那么使用如下变量计算真实的宽高值
        int width = 0;
        int height = 0;

        //每一行的宽高
        int lineWidth = 0;
        int lineHeight = 0;

        int childrenCount = getChildCount();
        for (int i = 0; i < childrenCount; i++) {
            View childView = getChildAt(i);
            //只有调用如果下的方法，放可以计算子视图的测量的宽高。
            measureChild(childView,widthMeasureSpec,heightMeasureSpec);


            int childWidth = childView.getMeasuredWidth();
            int childHeight = childView.getMeasuredHeight();

            MarginLayoutParams mp = (MarginLayoutParams) childView.getLayoutParams();

            if (lineWidth + childWidth + mp.leftMargin + mp.rightMargin <= widthSize){
                lineWidth += childWidth + mp.leftMargin + mp.rightMargin;
                lineHeight = Math.max(lineHeight,childHeight + mp.topMargin + mp.bottomMargin);
            }else {
                width = Math.max(width,lineWidth);
                height += lineHeight;

                lineWidth = childWidth + mp.leftMargin + mp.rightMargin;
                lineHeight = childHeight + mp.topMargin + mp.bottomMargin;
            }
            if (i == childrenCount - 1){
                width = Math.max(width,lineWidth);
                height += lineHeight;
            }

        }

        setMeasuredDimension((widthMode == MeasureSpec.EXACTLY) ? widthSize : width,(heightMode == MeasureSpec.EXACTLY) ? heightSize : height);
    }

    private List<List<View>> allviews = new ArrayList<>();
    private List<Integer> allHeight = new ArrayList<>();
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        //每一行的宽高值
        int lineWidth = 0;
        int lineHeight = 0;

        //提供一个集合，保存一行childview
        List<View> lineList = new ArrayList<>();
        int width = this.getMeasuredWidth();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childView = getChildAt(i);
            int childWidth = childView.getMeasuredWidth();
            int childHeight = childView.getMeasuredHeight();

            MarginLayoutParams mp = (MarginLayoutParams) childView.getLayoutParams();
            if (lineWidth + childWidth + mp.leftMargin + mp.rightMargin <= width){
                lineList.add(childView);
                lineWidth += childWidth + mp.leftMargin + mp.rightMargin;
                lineHeight = Math.max(lineHeight,childHeight + mp.topMargin + mp.bottomMargin);
            }else {
                allviews.add(lineList);
                allHeight.add(lineHeight);

                lineWidth = childWidth + mp.leftMargin + mp.rightMargin;
                lineHeight = childHeight + mp.topMargin + mp.bottomMargin;
                lineList = new ArrayList<>();
                lineList.add(childView);
            }

            if (i == childCount - 1){
                allviews.add(lineList);
                allHeight.add(lineHeight);
            }

        }

        int x = 0;
        int y = 0;
        for (int i = 0; i < allviews.size(); i++) {
            List<View> lineViews = allviews.get(i);
            for (int j = 0; j < lineViews.size(); j++) {
                View childView = lineViews.get(j);
                int childWidth = childView.getMeasuredWidth();
                int childHeigth = childView.getMeasuredHeight();

                MarginLayoutParams mp = (MarginLayoutParams)childView.getLayoutParams();
                int left = x + mp.leftMargin;
                int top = y + mp.topMargin;
                int right = left + childWidth;
                int bottom = top + childHeigth;

                childView.layout(left,top,right,bottom);
                x += childWidth + mp.leftMargin + mp.rightMargin;
            }
            y += allHeight.get(i);
            x = 0;
        }
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        MarginLayoutParams mp = new MarginLayoutParams(getContext(), attrs);
        return mp;
    }
}
