package com.example.financeapplication.common;

public class AppNetConfig {

//    public static final String baseUrl = "http://192.168.31.194:8080";
    public static final String baseUrl = "http://192.168.81.67:8080";
    public static final String index_url = baseUrl + "/P2PInvest/index";
    public static final String product_url = baseUrl + "/P2PInvest/product";
    public static final String login_url = baseUrl + "/P2PInvest/login";
}
