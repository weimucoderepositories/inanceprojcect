package com.example.financeapplication.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.provider.Settings;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.text.DecimalFormat;

public class MyProgressView extends View {
    private int width;
    private int height;

    private int roundColor = Color.GRAY;
    private int arcColor = Color.RED;
    private int textColor = Color.BLUE;
    private int roundWidth = UIUtils.db2px(10);
    private int textSize = UIUtils.db2px(10);

    private float maxProcess = 100;
    private float currentProcess = 60;
    private Paint paint;

    //这个是在JAVA代码中调用
    public MyProgressView(Context context) {
        this(context,null);
    }
    //下面是在xml中调用
    public MyProgressView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public MyProgressView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr,0);
    }

    public MyProgressView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        paint = new Paint();
        paint.setAntiAlias(true);
    }

    //测量
    //这个方法是为了 获取view的宽高
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

       width = MeasureSpec.getSize(widthMeasureSpec);
       height = MeasureSpec.getSize(heightMeasureSpec);
    }

    //画布
    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        //画圆还
        int cx = width/2 ;
        int cy = height/2 ;
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(roundColor);
        paint.setStrokeWidth(roundWidth);
        canvas.drawCircle(cx,cy,cy - roundWidth/2,paint);

        //话圆弧
        RectF rectF = new RectF(roundWidth/2,roundWidth/2,width - roundWidth/2,width - roundWidth/2);
        paint.setColor(arcColor);
        canvas.drawArc(rectF,0,currentProcess / maxProcess * 360,false,paint);

        //会文本

//        String.format( "%.2f",) ;
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        String progress =  decimalFormat.format(currentProcess / maxProcess * 100 ) + "%";
        Rect rect = new Rect();
        paint.getTextBounds(progress,0,progress.length(),rect);
        paint.setColor(textColor);
        paint.setStrokeWidth(0);
        paint.setTextSize(textSize);
        canvas.drawText(progress,width/2 - rect.width()/2, (float) (height * 1.0 /2.0) + rect.height()/2,paint);
    }
}
