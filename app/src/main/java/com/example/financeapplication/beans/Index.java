package com.example.financeapplication.beans;

import java.util.List;

public class Index {
    public List<BannerImage> imageArr;
    public ProductInfo proInfo;

    @Override
    public String toString() {
        return "Index{" +
                "imageArr=" + imageArr +
                ", proInfo=" + proInfo +
                '}';
    }
}
