package com.example.financeapplication.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.financeapplication.R;
import com.example.financeapplication.beans.Product;

import java.util.List;

public class MyProductAdapter extends BaseAdapter {

    private List<Product> productList;
    public MyProductAdapter(List<Product> productList){
        this.productList = productList;
    }

    @Override
    public int getCount() {
        if (productList == null){
            return 0;
        }
        return productList.size();
    }

    @Override
    public Object getItem(int position) {
        if (productList == null){
            return null;
        }
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {

            convertView = View.inflate(parent.getContext(), R.layout.product_list_item,null);
            viewHolder = new ViewHolder();

            viewHolder.pName = (TextView) convertView.findViewById(R.id.p_name);
            viewHolder.pMoney = (TextView) convertView.findViewById(R.id.p_money);
            viewHolder.pYearlv = (TextView) convertView.findViewById(R.id.p_yearlv);
            viewHolder.pSuodingdays = (TextView) convertView.findViewById(R.id.p_suodingdays);
            viewHolder.pMinzouzi = (TextView) convertView.findViewById(R.id.p_minzouzi);
            viewHolder.pMinnum = (TextView) convertView.findViewById(R.id.p_minnum);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Product product = productList.get(position);
        viewHolder.pName.setText(product.name);
        viewHolder.pMoney.setText(product.money);
        viewHolder.pYearlv.setText(product.yearRate);
        viewHolder.pSuodingdays.setText(product.suodingDays);
        viewHolder.pMinzouzi.setText(product.minTouMoney);
        viewHolder.pMinnum.setText(product.memberNum);

        return convertView;
    }

    private class ViewHolder {

        public TextView pName;
        public TextView pMoney;
        public TextView pYearlv;
        public TextView pSuodingdays;
        public TextView pMinzouzi;
        public TextView pMinnum;
    }
}
