package com.example.financeapplication.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.financeapplication.R;
import com.example.financeapplication.common.DrawUtils;
import com.example.financeapplication.common.FlowLayout;
import com.example.financeapplication.common.UIUtils;

import java.util.Random;

public class HotInvestFrament extends Fragment {

    //提供装配的数据
    private String[] datas = new String[]{"福利计划", "财神道计划", "硅谷钱包计划", "30天理财计划(加息2%)", "180天理财计划(加息5%)", "月月升理财计划(加息10%)",
            "中情局投资商业经营", "大学老师购买车辆", "屌丝下海经商计划", "美人鱼影视拍摄投资", "Android培训老师自己周转", "养猪场扩大经营",
            "旅游公司扩大规模", "铁路局回款计划", "屌丝迎娶白富美计划"
    };

    private FlowLayout flowLayout;
    private Random random = new Random();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_hot_invest, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        flowLayout = (FlowLayout) view.findViewById(R.id.flow_layout);

        for (int i = 0; i < datas.length; i++) {
            TextView textView = new TextView(getActivity());
            textView.setText(datas[i]);

            ViewGroup.MarginLayoutParams mp = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);

            mp.leftMargin = UIUtils.db2px(5);
            mp.topMargin = UIUtils.db2px(5);
            mp.rightMargin = UIUtils.db2px(5);
            mp.bottomMargin = UIUtils.db2px(5);
            textView.setLayoutParams(mp);

            int padding = UIUtils.db2px(5);
            textView.setPadding(padding,padding,padding,padding);

            int red = random.nextInt(222);
            int greed = random.nextInt(222);
            int blue = random.nextInt(222);

            int rgb = Color.rgb(red,greed,blue);
//            textView.setBackground(DrawUtils.getDrawable(rgb,3));

            textView.setBackground(DrawUtils.getSelector(DrawUtils.getDrawable(rgb,3),DrawUtils.getDrawable(Color.WHITE,3)));
            textView.setEnabled(true);  //设置可点击
            //如果设置点击时间就是可以点击的
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(),textView.getText().toString(),Toast.LENGTH_SHORT).show();
                }
            });
            flowLayout.addView(textView);
        }
    }
}
