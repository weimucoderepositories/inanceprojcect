package com.example.financeapplication.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.financeapplication.R;
import com.example.financeapplication.beans.Index;
import com.example.financeapplication.common.AppNetConfig;
import com.example.financeapplication.common.MyApplication;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * create an   fragment.
 */
public class HomeFragment extends Fragment {

    private ImageView navLeftbackImg;
    private TextView navTitleTv;
    private ImageView navSettingImg;

    private ViewPager bannerViewpager;
    private TextView recommandTv;
    private TextView moneyRate;
    private Index index;
    private CirclePageIndicator myCirclePagerindicator;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initView(view);
        //初始化数据
        initDatas();
        return view;
    }

    private void initView(View view) {

        navLeftbackImg = (ImageView) view.findViewById(R.id.nav_leftback_img);
        navTitleTv = (TextView) view.findViewById(R.id.nav_title_tv);
        navSettingImg = (ImageView) view.findViewById(R.id.nav_setting_img);
        navTitleTv.setText("首页");

        navLeftbackImg.setVisibility(View.INVISIBLE);
        navSettingImg.setVisibility(View.INVISIBLE);

        bannerViewpager = (ViewPager) view.findViewById(R.id.banner_viewpager);
        recommandTv = (TextView) view.findViewById(R.id.recommand_tv);
        moneyRate = (TextView) view.findViewById(R.id.money_rate);

        myCirclePagerindicator = (CirclePageIndicator) view.findViewById(R.id.my_circle_pagerindicator);

    }

    private void initDatas() {

        OkHttpClient httpClient = new OkHttpClient();
        String indexUrl = AppNetConfig.index_url;
        final Request request = new Request.Builder()
                .url(indexUrl)
                .get()
                .addHeader("Content-Type", "application/json")
                .build();
        Call call = httpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    Gson gson = new Gson();
                    //这里的值是string而不是toString
//                   response.body().string() 只能调用一次，调用一次之后 这个流就关闭了，调用两次会报错
                    String bodyStr = response.body().string();
                    index = gson.fromJson(bodyStr, new TypeToken<Index>() {
                    }.getType());
                    //response.body().string()不能调用两次
                    Log.d("TAG", "onResponse: " + ResponseBody.create(bodyStr, MediaType.parse("application/json")).string());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setUIDatas(index);
                        }
                    });

                }
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {


//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getActivity(), "error" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//                    }
//                });
//                bannerViewpager.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getActivity(), "error" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//                    }
//                });
                //主线程
                MyApplication.handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "error" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

                Log.d("TAG", "error: " + e.getLocalizedMessage());
            }

        });

    }

    //设置界面数据
    private void setUIDatas(Index index) {
        recommandTv.setText(index.proInfo.name);
        moneyRate.setText(index.proInfo.yearRate);

        bannerViewpager.setAdapter(new MyAdapter());
        myCirclePagerindicator.setViewPager(bannerViewpager);
    }

    class MyAdapter extends PagerAdapter {


        @Override
        public int getCount() {
            if (index != null) {
                return index.imageArr.size();
            }
            return 0;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            ImageView imageView = new ImageView(getActivity());
            String url = index.imageArr.get(position).IMAURL;

            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            Picasso.get().load(Uri.parse(url)).into(imageView);
            container.addView(imageView);
            return imageView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);

        }
    }


}