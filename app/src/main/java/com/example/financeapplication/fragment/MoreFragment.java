package com.example.financeapplication.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.financeapplication.R;

public class MoreFragment extends Fragment {

    private ImageView navLeftbackImg;
    private TextView navTitleTv;
    private ImageView navSettingImg;
    private TextView tvMoreRegist;
    private ToggleButton toggleMore;
    private TextView tvMoreReset;
    private RelativeLayout rlMoreContact;
    private TextView tvMorePhone;
    private TextView tvMoreFankui;
    private TextView tvMoreShare;
    private TextView tvMoreAbout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        navLeftbackImg = (ImageView) view.findViewById(R.id.nav_leftback_img);
        navTitleTv = (TextView) view.findViewById(R.id.nav_title_tv);
        navSettingImg = (ImageView) view.findViewById(R.id.nav_setting_img);

        navLeftbackImg.setVisibility(View.INVISIBLE);
        navSettingImg.setVisibility(View.INVISIBLE);
        navTitleTv.setText("更多");

        tvMoreRegist = (TextView) view.findViewById(R.id.tv_more_regist);
        toggleMore = (ToggleButton) view.findViewById(R.id.toggle_more);
        tvMoreReset = (TextView) view.findViewById(R.id.tv_more_reset);
        rlMoreContact = (RelativeLayout) view.findViewById(R.id.rl_more_contact);
        tvMorePhone = (TextView) view.findViewById(R.id.tv_more_phone);
        tvMoreFankui = (TextView) view.findViewById(R.id.tv_more_fankui);
        tvMoreShare = (TextView) view.findViewById(R.id.tv_more_share);
        tvMoreAbout = (TextView) view.findViewById(R.id.tv_more_about);
    }
}
