package com.example.financeapplication.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.financeapplication.R;
import com.example.financeapplication.common.UIUtils;
import com.example.financeapplication.common.randomLayout.StellarMap;

import java.util.Random;

/**
 *
 */
public class RecommandInvestFragment extends Fragment {

    //提供装配的数据
    private String[] datas = new String[]{"新手福利计划", "财神道90天计划", "硅谷钱包计划", "30天理财计划(加息2%)", "180天理财计划(加息5%)", "月月升理财计划(加息10%)",
            "中情局投资商业经营", "大学老师购买车辆", "屌丝下海经商计划", "美人鱼影视拍摄投资", "Android培训老师自己周转", "养猪场扩大经营",
            "旅游公司扩大规模", "铁路局回款计划", "屌丝迎娶白富美计划"
    };
    //声明两个子数组
    private String[] oneDatas = new String[datas.length / 2];
    private String[] twoDatas = new String[datas.length - datas.length / 2];
    private Random random = new Random();
    private StellarMap stellarMap;

    public RecommandInvestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recommand_invest, container, false);
        stellarMap = (StellarMap) view.findViewById(R.id.stellar_map);

        initDatas();
        return view;
    }

    private void initDatas() {
        //初始化子数组的数据
        for (int i = 0; i < datas.length; i++) {
            if (i < datas.length / 2) {
                oneDatas[i] = datas[i];
            } else {
                twoDatas[i - datas.length / 2] = datas[i];
            }
        }

        MyStellerMapAdapter mapAdapter = new MyStellerMapAdapter();
        stellarMap.setAdapter(mapAdapter);
        //设置稀疏度
        stellarMap.setRegularity(5,7);
        //设置默认组
        stellarMap.setGroup(0,true);
        //设置距离四周的位置
        stellarMap.setInnerPadding(UIUtils.db2px(10),UIUtils.db2px(10),UIUtils.db2px(10),UIUtils.db2px(10));


    }

    class MyStellerMapAdapter implements StellarMap.Adapter {

        @Override
        public int getGroupCount() {
            return 2;
        }

        @Override
        public int getCount(int group) {
            if (group == 0){
                return oneDatas.length;
            }else {
                return twoDatas.length;
            }

        }

        @Override
        public View getView(int group, int position, View convertView) {
            TextView textView = new TextView(getActivity());
            if (group == 0){
                textView.setText(oneDatas[position]);
            }else {
                textView.setText(twoDatas[position]);
            }
            //设置字体大小
            textView.setTextSize(UIUtils.db2px(5) + UIUtils.db2px(random.nextInt(10)));
            //颜色随机
            textView.setTextColor(Color.rgb(random.nextInt(211),random.nextInt(211),random.nextInt(211)));

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(),textView.getText().toString(),Toast.LENGTH_SHORT).show();
                }
            });
            return textView;
        }
        //没用到不用管
        @Override
        public int getNextGroupOnPan(int group, float degree) {
            return 0;
        }
        //下一组的缩放组
        @Override
        public int getNextGroupOnZoom(int group, boolean isZoomIn) {
            if (group == 0){  //如果当前是0 组 返回 1 组
                return  1;
            }else { //如果当前是1 组 返回 0 组
                return 0;
            }

        }
    }
}