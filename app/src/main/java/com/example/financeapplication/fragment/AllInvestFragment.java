package com.example.financeapplication.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.financeapplication.R;
import com.example.financeapplication.adapter.MyProductAdapter;
import com.example.financeapplication.beans.Product;
import com.example.financeapplication.common.AppNetConfig;
import com.example.financeapplication.common.MyProgressView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AllInvestFragment extends Fragment {

    private TextView marqueeTv;
    private ListView allInvestListview;
    private List<Product> productList;
    private MyProductAdapter myadpater;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_all_invest, container, false);
        marqueeTv = (TextView) view.findViewById(R.id.marquee_tv);
        allInvestListview = (ListView) view.findViewById(R.id.all_invest_listview);

        getDatas();
        return view;
    }


    private void getDatas() {
        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(AppNetConfig.product_url)
                .get()
                .addHeader("Content-Type", "application/json")
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                String resposeStr = response.body().string();
                try {
                    JSONObject jsonObject = new JSONObject(resposeStr);
                    String dataStr = jsonObject.getString("data");
                    Gson gson = new Gson();
                    productList = gson.fromJson(dataStr, new TypeToken<List<Product>>() {
                    }.getType());

                    allInvestListview.post(new Runnable() {
                        @Override
                        public void run() {
                            myadpater = new MyProductAdapter(productList);
                            allInvestListview.setAdapter(myadpater);
                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {

            }

        });
    }

}
