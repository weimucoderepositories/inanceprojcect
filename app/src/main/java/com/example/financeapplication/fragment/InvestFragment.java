package com.example.financeapplication.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.financeapplication.R;
import com.viewpagerindicator.TabPageIndicator;
import com.viewpagerindicator.TitlePageIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class InvestFragment extends Fragment {

    private ImageView navLeftbackImg;
    private TextView navTitleTv;
    private ImageView navSettingImg;
    private TabPageIndicator myTabpageIndicator;
    private ViewPager investViewpager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invest, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        navLeftbackImg = (ImageView) view.findViewById(R.id.nav_leftback_img);
        navTitleTv = (TextView) view.findViewById(R.id.nav_title_tv);
        navSettingImg = (ImageView) view.findViewById(R.id.nav_setting_img);

        navLeftbackImg.setVisibility(View.INVISIBLE);
        navSettingImg.setVisibility(View.INVISIBLE);
        navTitleTv.setText("投资");

        myTabpageIndicator = (TabPageIndicator) view.findViewById(R.id.my_tabpageIndicator);
        investViewpager = (ViewPager) view.findViewById(R.id.invest_viewpager);

        initFragmentDatas();

        FragmentManager fragmentManager = getFragmentManager();
        MyAdapter adapter = new MyAdapter(fragmentManager);
        investViewpager.setAdapter(adapter);
        myTabpageIndicator.setViewPager(investViewpager,0);
    }

    private List<Fragment> fragments = new ArrayList<>();
    private void initFragmentDatas() {

        AllInvestFragment allInvestFragment = new AllInvestFragment();
        HotInvestFrament hotInvestFrament = new HotInvestFrament();
        RecommandInvestFragment recommandInvestFragment = new RecommandInvestFragment();
        fragments.add(allInvestFragment);
        fragments.add(recommandInvestFragment);
        fragments.add(hotInvestFrament);
    }

    class MyAdapter extends FragmentPagerAdapter{

        public MyAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            String[] strings = getResources().getStringArray(R.array.invest_tab_arr);
            return strings[position];
        }
    }
}