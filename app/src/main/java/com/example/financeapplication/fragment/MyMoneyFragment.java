package com.example.financeapplication.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.financeapplication.R;
import com.example.financeapplication.activity.LoginActivity;
import com.example.financeapplication.activity.UserInfoActivity;
import com.example.financeapplication.common.BitmapUtils;
import com.example.financeapplication.common.MD5Utils;
import com.example.financeapplication.common.UIUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

/**
 *
 */
public class MyMoneyFragment extends Fragment implements View.OnClickListener {

    private ImageView navLeftbackImg;
    private TextView navTitleTv;
    private ImageView navSettingImg;
    private RelativeLayout rlMe;
    private RelativeLayout rlMeIcon;
    private ImageView ivMeIcon;
    private TextView tvMeName;
    private ImageView recharge;
    private ImageView withdraw;
    private TextView llTouzi;
    private TextView llTouziZhiguan;
    private TextView llZichan;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mymoney, container, false);
        initView(view);
        isLogin();
        return view;
    }

    private void initView(View view) {
        navLeftbackImg = (ImageView) view.findViewById(R.id.nav_leftback_img);
        navTitleTv = (TextView) view.findViewById(R.id.nav_title_tv);
        navSettingImg = (ImageView) view.findViewById(R.id.nav_setting_img);

        navLeftbackImg.setVisibility(View.INVISIBLE);
        navSettingImg.setVisibility(View.VISIBLE);
        navTitleTv.setText("我的资产");

        rlMe = (RelativeLayout) view.findViewById(R.id.rl_me);
        rlMeIcon = (RelativeLayout) view.findViewById(R.id.rl_me_icon);
        ivMeIcon = (ImageView) view.findViewById(R.id.iv_me_icon);
        tvMeName = (TextView) view.findViewById(R.id.tv_me_name);
        recharge = (ImageView) view.findViewById(R.id.recharge);
        withdraw = (ImageView) view.findViewById(R.id.withdraw);
        llTouzi = (TextView) view.findViewById(R.id.ll_touzi);
        llTouziZhiguan = (TextView) view.findViewById(R.id.ll_touzi_zhiguan);
        llZichan = (TextView) view.findViewById(R.id.ll_zichan);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user_info", Context.MODE_PRIVATE);
        String name = sharedPreferences.getString("name","");
        tvMeName.setText(name);
        String imageurl = sharedPreferences.getString("imageurl","");

        navSettingImg.setOnClickListener(this);

        Picasso.get().load(Uri.parse(imageurl)).transform(new Transformation() {
            @Override
            public Bitmap transform(Bitmap source) {
                Bitmap bitmap = BitmapUtils.zoom(source,64,64);
                bitmap = BitmapUtils.circleBitmap(bitmap);
                source.recycle();
                return bitmap;
            }

            @Override
            public String key() {
                return "ddd";
            }
        }).into(ivMeIcon);

        tvMeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void isLogin(){
        SharedPreferences preferences = getActivity().getSharedPreferences("user_info", Context.MODE_PRIVATE);
        String name = preferences.getString("name","");
        String phone = preferences.getString("phone","");

        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(phone)) {
            new AlertDialog.Builder(getActivity())
                    .setTitle("未登录")
                    .setMessage("请登录")
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).create().show();
        } else {

        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.nav_setting_img){
             Intent intent = new Intent(getActivity(), UserInfoActivity.class);
             startActivity(intent);
        }
    }
}